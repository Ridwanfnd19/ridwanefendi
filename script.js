function form() {
    let nama = document.forms["dataDiri"]["nama"].value;
    let usia = document.forms["dataDiri"]["usia"].value;
    let gender = document.forms["dataDiri"]["gender"].value;
    let pekerjaan = document.forms["dataDiri"]["pekerjaan"].value;
    let alamat = document.forms["dataDiri"]["alamat"].value;

    if ((nama == "") || (usia == "") || (gender == "")) {
        return false
    } else {
        sessionStorage.setItem("nama", nama);
        sessionStorage.setItem("usia", usia);
        sessionStorage.setItem("gender", gender);
        sessionStorage.setItem("pekerjaan", pekerjaan);
        sessionStorage.setItem("alamat", alamat);
        alert("Data berhasil disimpan"); 
    }
}

let hasil = document.getElementById("hasil");
let dataNama = sessionStorage.getItem("nama");
let dataUsia = sessionStorage.getItem("usia");
let dataGender = sessionStorage.getItem("gender");
let dataPekerjaan = sessionStorage.getItem("pekerjaan");
let dataAalamat = sessionStorage.getItem("alamat");

if ((dataNama != null) || (dataUsia != null) || (dataGender != null)) {
    let text = `Nama : ${dataNama}<br>Usia : ${dataUsia} Tahun<br>Jenis Kelamin : ${dataGender}<br>`;

    if ((dataPekerjaan != "") && (dataPekerjaan != null)) {
        text += `Pekerjaan : ${dataPekerjaan}<br>`;
    } 
    
    if ((dataAalamat != "") && (dataAalamat != null)) {
        text += `Alamat : ${dataAalamat}`;
    }

    hasil.innerHTML = text;
} else {
    hasil.innerHTML = "Data masih kosong";
}

function reset() {
    sessionStorage.removeItem("nama");
    sessionStorage.removeItem("usia");
    sessionStorage.removeItem("gender");
    sessionStorage.removeItem("pekerjaan");
    sessionStorage.removeItem("alamat");
    alert("Data berhasil di reset");
    location.reload();
}